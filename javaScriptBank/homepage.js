var urlFixed = "http://localhost:8080/javabank5/api/customer";
var appJson = "application/json";

var customerData;

$(document).ready(function () {
  request();

  $("#add").click(function (event) {
    addCustomer();
  });
});

function request() {
  $.ajax({
    url: urlFixed,
    type: "GET",
    async: true,
    contentType: appJson,
    success: successCallback,
    error: errorCallback,
  });
}

function successCallback(response) {
  createTable(response);
}

function errorCallback(request, status, error) {
  console.log("Request failed!");
  console.log(request, status, error);
}

function createTable(customerData) {
  var table = $("#table");

  customerData.forEach(function (customer) {
    var row =
      "<tr class='tableRow'>" +
      "<td>" +
      customer.id +
      "</td>" +
      "<td>" +
      customer.firstName +
      "</td>" +
      "<td>" +
      customer.lastName +
      "</td>" +
      "<td>" +
      customer.email +
      "</td>" +
      "<td>" +
      customer.phone +
      "</td>" +
      '<td><button type="button" id="edit-btn-' +
      customer.id +
      '" class="edit-btn btn btn-success">edit</button></td>' +
      '<td><button type="button" id="remove-btn-' +
      customer.id +
      '" class="remove-btn btn btn-danger">delete</button></td>' +
      "</tr>";

    $(row).appendTo(table);

    $("#edit-btn-" + customer.id).click(function (event) {
      updateInput(customer);
    });

    $("#remove-btn-" + customer.id).click(function (event) {
      deleteCustomer(customer.id);
    });
  });
}

function editCustomer() {
  $.ajax({
    url: urlFixed,
    type: "PUT",
    data: JSON.stringify({
      firstName: $("#firstName").val(),
      lastName: $("#lastName").val(),
      email: $("#email").val(),
      phone: $("#phone").val(),
    }),
    async: true,
    contentType: appJson,
    success: updateTable,
    error: errorCallback,
  });
}

function addCustomer() {
  $.ajax({
    url: urlFixed,
    type: "POST",
    data: JSON.stringify({
      firstName: $("#firstName").val(),
      lastName: $("#lastName").val(),
      email: $("#email").val(),
      phone: $("#phone").val(),
    }),
    async: true,
    contentType: appJson,
    success: updateTable,
    error: errorCallback,
  });
}

function updateTable(response) {
  resetTable();
  request();
}

function resetTable() {
  $(".tableRow").remove();
}

function deleteCustomer(id) {
  $.ajax({
    url: urlFixed + "/" + id,
    type: "DELETE",
    async: true,
    contentType: appJson,
    success: updateTable,
    error: errorCallback,
  });
}

function updateInput(customer) {

    var customerId=customer.id;
    console.log(customer.id);

    $(".firstName").val(""+customer.firstName);
    $(".lastName").val(customer.lastName);
    $(".email").val(customer.email);
    $(".phone").val(customer.phone);
}
