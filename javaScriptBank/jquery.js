$(document).ready(function () {

    $('.button').click(addKebab);

    sendRequest();

});

function sendRequest(){

    $.ajax({
        url: "http://localhost:8080/javabank5/api/customer",
        async: true,
        success: successCallback,
        error: errorCallback,
    });
};

function resetTable(){

    $(".itemRow").remove();
}

function errorCallback(request, status, error) {

    console.log("something went wrong with the ajax request");
    console.log(request, status, error);
}

function successCallback(response) {

    createTable(response);
}

function kebabCallback() {

    resetTable();
    sendRequest();
}

function addKebab(){

    $.ajax({
        url: 'http://localhost:8080/javabank5/api/customer',
        type: 'POST',
        data: JSON.stringify({
            firstName: 'kebab',
            lastName: 'LORD',
            email: 'superkabob@academiadecodigo.org',
            phone: '9812399759'
        }),
        async: true,
        contentType: 'application/json',
        success: kebabCallback,
        error: errorCallback
    });
}

function createTable(customerData) {
    console.log(customerData);
    var table = $("#table");

    customerData.forEach(function (customer) {
        var row =
            '<tr class="itemRow">'+
            "<td>" +
            customer.id +
            "</td>" +
            "<td>" +
            customer.firstName +
            "</td>" +
            "<td>" +
            customer.lastName +
            "</td>" +
            "<td>" +
            customer.email +
            "</td>" +
            "<td>" +
            customer.phone +
            "</td></tr>";
        $(row).appendTo(table);
    });
}

var customerData = [
    {
        id: 1,
        firstName: "Rui",
        lastName: "Ferrão",
        email: "rui@gmail.com",
        phone: "777888",
    },
    {
        id: 2,
        firstName: "Sergio",
        lastName: "Gouveia",
        email: "sergio@gmail.com",
        phone: "777999",
    },
    {
        id: 3,
        firstName: "Bruno",
        lastName: "Ferreira",
        email: "bruno@gmail.com",
        phone: "777666",
    },
    {
        id: 4,
        firstName: "Rodolfo",
        lastName: "Matos",
        email: "rodolfo@gmail.com",
        phone: "777333",
    },
];
