//Data types
//primitives 

//string 'good' "goooooog"
//number -1.5, +Infinity, 1.7e9, NaN 
//boolean true/false
//null
//undefined -void returns undefined

var sum= 0.1 + 0.2;
var sum2 = 0.1 + 0.4;
var div = 1/3;

console.log(sum);
console.log(sum2);
console.log(div);

//NON PRIMITIVE
//An object

var date = {
    year: 2020,
    month: 'April',
    day: 2
};

var pika = {};

date.day = 12;
date.leapYear = true;

delete date.day;

var key = 'hour';
date[key] = 18;
date['seconds'] = 12;

// console.log(date.hour);
// console.log(date);

//References
var age = 16;
var age2 = 16;

var felynx = {
    date:date,
    isTrol:true,
    sound: function(){
        console.log('prrrrrrrr')
    }
};

var felynx2 = felynx;

felynx.isHungry = true;

console.log(felynx2);

console.log(typeof undefined);
console.log(typeof null);

//TYPE COERCION

//manbo jambo, be careful

//functions
var sound = function(){
    console.log('rawr');
    return 'rawr';
}

function soundDifferent(){
    console.log('loool')
}

sound.volume = '30db';

typeof sound;             //function
typeof sound();           //string
typeof sound.pitch;       //string
typeof soundDifferent();  //undefined

//cenas fudidas
var talk = "kebab station";
var objTalk = new String(talk);

console.log(objTalk);

// scope
// global scope
// local scope

var puff = 'stack';

var sleepOnPuffs = function(){
    puff = 'lol'; //this is global scope
}

console.log(puff);
console.log(typeof console.log);

// forEach only works on arrays

Object.keys(felynx).forEach(
    function(key){
        console.log(felynx2[key]);
    }
);

// Functional Style with arrays
// MAP
// all the elements on the array

// FILTER
// some elemnts of the array 
// works just like in java, but instead of lambda, u pass an actual function

// REDUCE
// bat shit ( crazy ) very good
// acc will take the value returned from the last return, 
// if u wanna accumulate, u must pass him on the return
