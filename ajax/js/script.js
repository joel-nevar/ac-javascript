var API_URL = 'http://localhost:8080/javabank5/api/customer';

$(document).ready(function () {

    $('.btn-add').click(postCustomer);

    sendRequest();

});

function sendRequest(){

    $.ajax({
        url: API_URL,
        async: true,
        success: successCallback,
        error: errorCallback,
    });
};

function resetTable(){

    $(".customer-data").remove();
}

function errorCallback(request, status, error) {

    console.log("something went wrong with the ajax request");
    console.log(request, status, error);
}

function successCallback(response) {

    populateCustomers(response);
}

function successPost() {

    resetTable();
    sendRequest();
}

function addButton(){

}

function postCustomer(){

    $.ajax({
        url: API_URL,
        type: 'POST',
        data: JSON.stringify({
            //TODO
            firstName: '',
            lastName: 'LORD',
            email: 'superkabob@academiadecodigo.org',
            phone: '9812399759'
        }),
        async: true,
        contentType: 'application/json',
        success: successPost,
        error: errorCallback
    });
}

function populateCustomers(customerData) {

    var elementStr;
    var customersTable = document.getElementById("customer-table");
    var row;

    customerData.forEach(function(element) {
        elementStr =
            "<td>" +
            element.firstName +
            "</td>" +

            "<td>" +
            element.lastName +
            "</td>" +

            "<td>" +
            element.email +
            "</td>" +

            "<td>" +
            element.phone +
            "</td>" +

            '<td><button type="button" id="edit-btn-' +
            element.id +
            '" class="edit-btn btn btn-success">edit</button></td>' +
            '<td><button type="button" id="remove-btn-' +
            element.id +
            '" class="remove-btn btn btn-danger">delete</button></td>';

        row = customersTable.insertRow(-1);
        row.innerHTML = elementStr;

        row.setAttribute("id", "custumer-" + element.id);
        row.setAttribute("class", "customer-data");
    });
}
