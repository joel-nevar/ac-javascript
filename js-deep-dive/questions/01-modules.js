/**
 * Creates a counter module with an initial value, zero if not provided
 */
exports.createCounter = function(counter) {

    var count = counter;

    if(count === undefined){
        count = 0;
    }

    return {
        get: function(){
            return count;
        },
        reset: function() {
            count = 0;
        },
        increment: function(){
            count += 1;
        }
    }
};

/**
 * Creates a person module with name and age
 * An initial name value should be provided and
 * an exception thrown if not
 */
exports.createPerson = function(name) {

    if(name == undefined || name == ''){
        throw new Error("");
    }

    var person = {
        age: 0,
        name: name,
    }

    return {
        getName: function (){
            return person.name;
        },
        getAge: function(){
            return person.age;
        },
        setAge:function(age){
            person.age = age;
        }
    };
};
